#!/usr/bin/env python3

__author__ = "Robert McHardy"
__email__ = "robert@robertmchardy.de"
__license__ = "MIT"

from nltk.tokenize import sent_tokenize, word_tokenize
import numpy
import gensim

# Adjust variables to your system.
w2v_model = "/mount/arbeitsdaten/studenten1/mchardrt/satire/misc/german_model"
vector_model = gensim.models.Word2Vec.load(w2v_model).wv
satire_set = {"der-postillon", "zynismus24", "der-zeitspiegel", "eulenspiegel-zeitschrift", "satirepatzer",
              "eine-zeitung", "titanic-magazin", "der-enthueller", "norddeutsche-nachrichten",
              "dietagespresse", "welt"}
publication_set = {"der-postillon": 0, "zynismus24": 1, "der-zeitspiegel": 2, "eulenspiegel-zeitschrift": 3,
                   "satirepatzer": 4, "eine-zeitung": 5, "titanic-magazin": 6, "der-enthueller": 7,
                   "norddeutsche-nachrichten": 8, "dietagespresse": 9, "welt": 10, "zeit": 11, "sueddeutsche": 12,
                   "spiegel": 13, "derstandard": 14}


def generate_text_vec(text):
    """
    Generates a numpy array consisting of word2vec vectors for each word in the text.
    :param text: The tokenized document.
    :return: List of word2vec vectors.
    """
    return numpy.array([vector_model[word] for word in text if word in vector_model])


def construct_feature_vector(text):
    """
    Constructs the word2vec embeddings of the words contained in the document.
    :param text: The document (raw/not tokenzied).
    :return: A numpy vector consisting of the word embeddings.
    """
    # Split text in sentences.
    sentences = sent_tokenize(text, language="german")
    # Split sentences in words.
    words = []
    for sentence in sentences:
        words += word_tokenize(sentence, language="german")
    return generate_text_vec(words)


def get_label_data(doc_publication):
    """
    Return integer encoded label data. The first element represents if the document is satire (1) or not (0), the second
    element represents the publication.
    :param doc_publication:
    :return: Integer encoded label data.
    """
    if doc_publication in satire_set:
        return 1, publication_set[doc_publication]
    else:
        return 0, publication_set[doc_publication]


def get_contents_with_class(text, pub):
    sentences = sent_tokenize(text, language="german")
    words = []
    for sentence in sentences:
        words += word_tokenize(sentence, language="german")
    if pub in satire_set:
        return words, 1
    return words, 0
