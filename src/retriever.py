#!/usr/bin/env python3
__author__ = "Robert McHardy"
__email__ = "robert@robertmchardy.de"
__license__ = "MIT"

import os, feature_extractors, numpy, classifier, random, sqlite3
from keras.utils import to_categorical, normalize
from keras_preprocessing.sequence import pad_sequences
from multiprocessing import Pool

publications = ["der-postillon", "zynismus24", "der-zeitspiegel", "eulenspiegel-zeitschrift", "satirepatzer",
                "eine-zeitung", "titanic-magazin", "der-enthueller", "norddeutsche-nachrichten",
                "dietagespresse", "welt", "zeit", "sueddeutsche", "spiegel", "derstandard"]

# Helper functions for generating word embeddings with multiprocessing.
def helper(doc_data):
    return [feature_extractors.construct_feature_vector(doc_data[0]), feature_extractors.get_label_data(doc_data[1])]


def helper2(doc_data):
    return feature_extractors.get_contents_with_class(doc_data[0], doc_data[1])


class Retriever:
    def __init__(self, database):
        """
        Initializes a retriever for the articles in the corpus. The retriever should not be used directly. Use
        the BatchGenerator class instead, which allows to generate batches of arbitrary size.
        :param database: Path to the SQLite3 database where the corpus is stored.
        """
        print("Establishing database connection.")
        self.connection = sqlite3.connect(database)
        self.cursor = self.connection.cursor()
        print("Reading protected IDs.")
        self.protected_ids = set()
        with open("../data/evaluation_ids", "r") as f:
            for line in f:
                self.protected_ids.add(int(line.strip()))

    def close(self):
        # Close the connection to the database.
        self.cursor.close()
        self.connection.close()

    def get_docs_by_publication(self, publication, ids=None):
        """
        Returns all indexed documents by a specific publication.
        """
        if ids is None:
            ids = self.protected_ids
        return [doc for doc in self.cursor.execute(f"SELECT * FROM articles WHERE publication='{publication}'")
                if doc[0] not in ids]

    def get_docs_by_publication_and_ids(self, publication, ids):
        """
        Returns all indexed documents by a specific publication if they are contained in the given ids.
        """
        return [doc for doc in self.cursor.execute(f"SELECT * FROM articles WHERE publication='{publication}'")
                if doc[0] in ids]

    def get_docs_by_id(self, id):
        """
        Returns (all) the document(s) which match(es) the given id .
        """
        return [doc for doc in self.cursor.execute(f"SELECT * FROM articles WHERE id='{id}'")]

    def get_all_docs(self):
        """
        Returns all documents in the index.
        """
        return [doc for doc in self.cursor.execute(f"SELECT * FROM articles")]

    def get_protected_docs(self):
        """
        Returns all protected documents.
        """
        return [doc for doc in self.cursor.execute(f"SELECT * FROM articles") if doc[0] in self.protected_ids]

    def get_unprotected_docs(self):
        return [doc for doc in self.cursor.execute(f"SELECT * FROM articles") if doc[0] not in self.protected_ids]

    def get_docs_with_classes(self, ids, protected=False, docs=None):
        doc_data = []
        if not docs:
            if protected:
                print("Acquiring protected documents.")
                docs = [doc for doc in self.get_protected_docs() if doc[0] in ids]
            else:
                print("Acquiring unprotected documents.")
                docs = [doc for doc in self.get_unprotected_docs() if doc[0] in ids]
        for doc in docs:
            doc_data.append((doc[6], doc[3]))
        del docs
        # Generate embeddings and one-hot vectors.
        with Pool(80) as p:
            data = p.map(helper, doc_data)
            x_embedding, y_satire, y_publications = [], [], []
            x_embedding = pad_sequences([data_point[0] for data_point in data], maxlen=500, padding="post",
                                        truncating="post", dtype='float32', value=0.0)
            x_embedding = numpy.array(x_embedding)
            for index, data_point in enumerate(data):
                y_satire.append(data_point[1][0])
                y_publications.append(data_point[1][1])
        return x_embedding, to_categorical(y_satire, 2), to_categorical(y_publications, len(publications))

    def get_docs_with_contents(self, ids, protected=False):
        print("Acquiring unprotected documents.")
        if protected:
            docs = [doc for doc in self.get_protected_docs() if doc[0] in ids]
        else:
            docs = [doc for doc in self.get_unprotected_docs() if doc[0] in ids]
        doc_data = []
        for doc in docs:
            doc_data.append((doc[6], doc[3]))
        with Pool(80) as p:
            data = p.map(helper2, doc_data)
        return data

    def get_doc_classes(self, ids, protected=False):
        print("Acquiring unprotected documents.")
        if protected:
            docs = [doc for doc in self.get_protected_docs() if doc[0] in ids]
        else:
            docs = [doc for doc in self.get_unprotected_docs() if doc[0] in ids]
        with Pool(80) as p:
            data = p.map(feature_extractors.get_label_data, docs)
        return data


class BatchGenerator:
    def __init__(self, batch_size, database, publications=None):
        """
        The BatchGenerator allows to get batches of arbitrary size.
        :param batch_size: Size of a single batch.
        :param database: Path to the database.
        :param publications: Optional. Only specified publications will be present in the batches.
        """
        self.retriever = Retriever(database)
        self.batch_size = batch_size
        self.pub_lengths = {"der-postillon": 5065, "zynismus24": 402, "der-zeitspiegel": 171,
                       "eulenspiegel-zeitschrift": 192,
                       "satirepatzer": 193,
                       "eine-zeitung": 416, "titanic-magazin": 149, "der-enthueller": 324,
                       "norddeutsche-nachrichten": 211,
                       "dietagespresse": 1271, "welt": 1249, "zeit": 57802, "sueddeutsche": 177605, "spiegel": 31180,
                       "derstandard": 53632}
        self.max = sum(self.pub_lengths.values())
        self.validation_ids = set()
        if os.path.isfile("../data/validation_ids"):
            with open("../data/validation_ids", "r") as f:
                for line in f:
                    self.validation_ids.add(int(line.strip()))
        self.used_ids = self.retriever.protected_ids | self.validation_ids
        self.train_docs = self.max - len(self.used_ids)
        if publications is not None:
            m = 0
            self.docs = []
            for pub in publications:
                if pub in self.pub_lengths:
                    m += self.pub_lengths[pub]
                    self.docs += self.retriever.get_docs_by_publication(pub, self.used_ids)
            self.used_docs = []
            self.pubs = publications
            self.train_docs = m

    def generate_batch(self):
        """
        Generates a batch of the specified size.
        :return Numpy arrays consisting of the embeddings as well as their classes (satire/non-satire
        and their publication respectively; as one-hot encoded vectors).
        """
        docs = []
        batch_ids = set()
        remaining_docs = self.max - len(self.used_ids) - 1
        for i in range(min(self.batch_size, remaining_docs)):
            index = None
            # Obtain a valid, unused index.
            while index in self.used_ids or index is None:
                index = random.randint(0, self.max - 1)
            batch_ids.add(index)
            self.used_ids.add(index)
            # Get the corresponding document.
            doc = self.retriever.get_docs_by_id(index)
            docs.append((doc[0][6], doc[0][3]))
        if len(self.used_ids) >= self.max - self.batch_size / 2:
            # Reset the generator for the next epoch.
            print("Resetting the batch generator.")
            self.used_ids = self.retriever.protected_ids | self.validation_ids
        print("Generating input vectors in Generator.")
        # Generate the vectors.
        with Pool(80) as p:
            data = p.map(helper, docs)
            x_embedding, y_satire, y_publications = [], [], []
            x_embedding = pad_sequences([data_point[0] for data_point in data], maxlen=500, padding="post",
                                        truncating="post", dtype='float32', value=0.0)
            x_embedding = numpy.array(x_embedding)
            for index, data_point in enumerate(data):
                y_satire.append(data_point[1][0])
                y_publications.append(data_point[1][1])
        return x_embedding, to_categorical(y_satire, 2), to_categorical(y_publications, len(publications))

    def generate_publication_batch(self):
        """
        Generates a batch of the specified size containing only documents of the specified publications.
        :return Numpy arrays consisting of the embedding and feature vectors as well as their classes (satire/non-satire
        and their publication respectively; as one-hot encoded vectors).
        """
        docs = []
        for doc in self.docs:
            docs.append((doc[6], doc[3]))
        print("Generating input vectors in Generator.")
        # Generate the vectors.
        with Pool(80) as p:
            data = p.map(helper, docs)
            x_embedding, y_satire, y_publications = [], [], []
            x_embedding = pad_sequences([data_point[0] for data_point in data], maxlen=500, padding="post",
                                        truncating="post", dtype='float32', value=0.0)
            x_embedding = numpy.array(x_embedding)
            for index, data_point in enumerate(data):
                y_satire.append(data_point[1][0])
                y_publications.append(data_point[1][1])
        return x_embedding, to_categorical(y_satire, 2), to_categorical(y_publications, len(publications))

    def generate_validation_data(self, publications=None):
        """
        Returns all the validation data (does not depend on batch size).
        """
        if not os.path.isfile("../data/validation_ids"):
            with open("../data/validation_ids", "w") as f:
                for id in self.validation_ids:
                    f.write(str(id) + "\n")
        if not publications:
            return self.retriever.get_docs_with_classes(self.validation_ids)
        else:
            return self.retriever.get_docs_with_classes([], docs=self.retriever.get_docs_by_publication_and_ids(
                publications, self.validation_ids))

    def generate_test_data(self, publications=None):
        """
        Returns all the test (evaluation) data (does not depend on batch size).
        """
        if not publications:
            return self.retriever.get_docs_with_classes(self.retriever.protected_ids, protected=True)
        else:
            return self.retriever.get_docs_with_classes([], docs=self.retriever.get_docs_by_publication_and_ids(
                publications, self.retriever.protected_ids))

    def generate_training_data(self):
        return self.retriever.get_docs_with_classes([i for i in range(0, self.max) if not i in self.used_ids])

    def generate_test_data_contents(self):
        return self.retriever.get_docs_with_contents(self.retriever.protected_ids, protected=True)

    def generate_validation_data_contents(self):
        return self.retriever.get_docs_with_contents(self.validation_ids)

    def generate_training_data_contents(self):
        return self.retriever.get_docs_with_contents([i for i in range(0, self.max) if not i in self.used_ids])

    def generate_test_data_classes(self):
        return self.retriever.get_doc_classes(self.retriever.protected_ids, protected=True)

    def generate_validation_data_classes(self):
        return self.retriever.get_doc_classes(self.validation_ids)

    def has_docs(self):
        """
        :return: If the BatchGenerator still has documents in the current cycle.
        """
        return len(self.used_ids) < self.train_docs
