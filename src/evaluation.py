#!/usr/bin/env python3
__author__ = "Robert McHardy"
__email__ = "robert@robertmchardy.de"
__license__ = "MIT"

from keras import backend as K
import matplotlib, numpy
matplotlib.use('agg')
import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sn
import sklearn, numpy, itertools


def evaluate(tp, fp, tn, fn):
    """
    Calculate precision, recall and F-Score
    :param tp: amount of true positives.
    :param fp: amount of false positives.
    :param tn: amount of true negatives.
    :param fn: amount of false negatives.
    """
    precision = tp / (tp + fp + K.epsilon())
    recall = tp / (tp + fn + K.epsilon())
    f_score = 2 * (precision * recall) / (precision + recall + K.epsilon())
    accuracy = (tp + tn) / (tp + fp + tn + fn + K.epsilon())
    print("Precision:", precision, "\nRecall:", recall, "\nF-Score:", f_score, "\nAccuracy", accuracy, "\nTP:", tp,
          "\nFP:", fp, "\nTN:", tn, "\nFN:", fn)


def confusion_matrix(y_true, y_pred):
    return sklearn.metrics.confusion_matrix(y_true, y_pred)


def cm_s(y_true, y_pred):
    array = [[0 for x in range(15)], [0 for x in range(15)]]
    for i, y in enumerate(y_pred):
        array[y][y_true[i]] += 1
    return array


def f1_score(pre, rec):
    return 2 * (pre * rec) / (pre + rec + K.epsilon())


def metrics(y_true, y_pred):
    true_positives, true_negatives, false_positives, false_negatives = 0, 0, 0, 0
    for i, val in enumerate(y_true):
        correct_class = val
        if correct_class == 1 and y_pred[i] == 1:
            true_positives += 1
        elif correct_class == 0 and y_pred[i] == 0:
            true_negatives += 1
        elif correct_class == 1 and y_pred[i] == 0:
            false_negatives += 1
        elif correct_class == 0 and y_pred[i] == 1:
            false_positives += 1
    recall = true_positives / (true_positives + false_negatives + K.epsilon())
    precision = true_positives / (true_positives + false_positives + K.epsilon())
    accuracy = (true_positives + true_negatives) / (true_positives + false_positives + true_negatives + false_negatives
                                                    + K.epsilon())
    f1 = f1_score(precision, recall)
    print(f"TP: {true_positives}, FP: {false_positives}, TN: {true_negatives}, FN: {false_negatives}")
    return accuracy, precision, recall, f1


def plot_confusion_matrix(array1, array2, fn):
    classes = ["PO", "ZY", "ZE", "EU", "SA", "ZEI", "TI", "EN", "NO", "TA", "WE", "Z", "SÜ", "SP", "ST"]
    classes_rows = classes + ["", "News", "Satire"]
    blank = [[numpy.nan for x in range(15)]]
    df_cm = pd.DataFrame(array1 + blank + array2, index=classes_rows, columns=classes)
    df_cm2 = pd.DataFrame(array2, index=["News", "Satire"])
    fig, ax = plt.subplots(figsize=(14, 12))
    sn.heatmap(df_cm, annot=True, fmt=".0f", ax=ax, cmap="YlGn", vmin=0, vmax=1000)
    #sn.heatmap(df_cm2, annot=True, fmt="d", ax=ax2, cmap="YlGn", vmin=0, vmax=1000)
    plt.savefig(fn)


def multiclass_metrics(y_true, y_pred):
    accuracy = sklearn.metrics.accuracy_score(y_true, y_pred)
    prec, rec, f1 = sklearn.metrics.precision_recall_fscore_support(y_true, y_pred, average="weighted")[:3]
    f1 = (2 * prec * rec) / (prec + rec) if prec + rec > 0 else 0.0
    return accuracy, prec, rec, f1

