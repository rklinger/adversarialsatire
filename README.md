# Adversarial Training for Satire Detection: Controlling for Confounding Variables

This respository contains the code and the information how to access the data
for the paper "Adversarial Training for Satire Detection: Controlling for Confounding Variables" (accepted at
NAACL-HLT 2019). The preprint of the paper can be found on [arxiv.org](https://arxiv.org/abs/1902.11145).
More information is available on the website of the [IMS of the University of Stuttgart](https://www.ims.uni-stuttgart.de/forschung/ressourcen/korpora/germansatire.html).

# Prerequisites

Python 3 (= 3.6.8)  
NumPy (= 1.15.4)  
tensorflow-gpu (= 1.11.0)  
NLTK (= 3.3)  
Gensim (= 3.4.0)  
Keras (=2.2.4)  
spacy (=2.0.16)  
matplotlib (=3.0.3)  
pandas (=0.24.2)  
seaborn (=0.9.0)  
scikit-learn (=0.20.3)

Therefore, this might work:

    pip3 install numpy tensorflow-gpu nltk keras gensim spacy matplotlib pandas seaborn scikit-learn


# Obtain the code  
    git clone https://rklinger@bitbucket.org/rklinger/adversarialsatire.git

# Usage
The main file is ``classifier.py``. It can be run with different arguments depending on the mode it is run in.
The two modes are ``-t`` to train a new model and ``-e`` to evaluate a already trained model.  

Train arguments:  
``-d``: Path to the SQLite database containing the corpus. (Default: "./corpus.db")   
``-m``: Path to a partly trained model if the training should be continued.  
``-n``: Filename to store the model to. (Default: "model")  
``-s``: Weight applied to the gradient of the satire classifier part of the model. (Default: 1.0)  
``-p``: Weight applied to the gradient of the publication identifier part of the model. (Default: 1.0)  
``-a``: Weight applied in the gradient reversal layer. (Default: 0.0)  
``--publications``: List of publications to train the model on. (Default: all)  

Evaluate arguments:  
``-d``: Path to the SQLite database containing the corpus. (Default: "./corpus.db")  
``-m``: Path to a trained model that should be evaluated. (Default: "model")  

Arguments with a default value don't need to be specified. For instance, to train a model with adversarial training (lambda = 0.3) and store that model in a file called
"adv_lambda_03_model" run the following command:  

    python3 classifier.py -t -n adv_lambda_03_model -a 0.3

# Database scheme
The database consists out of seven columns with the following layout:

| id      | url     | pub_date | publication   | title | news_class | news_text  |
| --------|---------|----------|---------------|-------|------------|------------|
| 0       | http... | Di...    | der-postillon | abc   | satire     | Es komm... |         |


IDs and URLs are unique to articles. There is a total of 15 publications (i.e., "der-postillon") and two news classes
consisting out of "real" and "satire". The column "news_text" contains the untokenized text of an article, "title" the 
untokenized headline. An example database with fictional data can be found in the ``res`` folder of the repository. The
IDs of the train/dev/test sets used in the paper are contained in the corresponding lists in the ``res`` folder.

# Obtaining the data

We cannot publish the complete data for licensing reasons. We plan to make an example data set available soon which 
exemplifies the database structure. We propose that you recrawl the data as described in the paper. If you need help with
that, please let us know. We might also be able to provide you with the corpus via personal communication. If you already 
received a password from us, you can find the corpus [here](http://www.romanklinger.de/stuff/advsatire/protected/corpus_db.zip).

The URLs for all articles together with the ids of the train/val/test splits can be found in the data folder in the source of this project.
